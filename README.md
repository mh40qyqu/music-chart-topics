# music-chart-topics
## What is this about?
This project was created in the context of the course at university Leipzig: 'Introduction to Digital Humanities'.
Inside of this repository are methods and data to measure the lexical richness and create a semantical analysis of lyrics from chart music from 1950 to 2019.
The project is written in the Go programming language.
