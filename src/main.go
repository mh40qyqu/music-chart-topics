package main

import (
	"music-chart-topics/src/visualization"
)

const chartListFileName string = "../data/ChartList.tsv"

//ChartStartYear The start year of crawling for charts
const ChartStartYear = 1950

//ChartEndYear The end year for crawling for charts
const ChartEndYear = 2019

//ChartsPerSite The number of charts we want to crawl for every year
const ChartsPerSite = 40

// various file names and folder:
const (
	lyricsJSONFolder = "data/lyrics/"
)

func main() {
	//nlp.GetTopicsFromDecade(1990, util.ReadInStopWordsFromFile("data/topicStopwords.txt")...)
	//nlp.PrintTopics(util.ReadLyricsForDecade(2000), ldaconfig)
	//nlp.GenerateAllTopics()
	//nlp.PrintAllZipf()
	//visualization.GenerateBadWordsGraph(1950, 2019, "bad_words_from_all_years.png")
	//nlp.GetTopicsFromDecade(2000, util.ReadInStopWordsFromFile("data/topicStopwords.txt")...)
	//nlp.CleanAndSaveAllLyrics()
	//visualization.GenerateSeperateTopicWordDistributionGraph()
	//visualization.PrintTopicAllocationGraph()
	//visualization.GenerateTopicWordDistributionGraphByYear()
	//visualization.GenerateWordcloud("nouns_and_adjectives/", 2010, 90, "first_wordcloud.png")
	//visualization.GenerateBadWordsGraph(1950, 2019, "bad_words_from_all_years.png")
	//nlp.GetTopicsFromAllYears()
	//visualization.GenerateTopicDiversityGraph()
	//visualization.GenerateGraphsOfSelectedTopics([]string{"Violence", "Wealth"})
	visualization.GenerateSeperateTopicWordDistributionGraph()
}
