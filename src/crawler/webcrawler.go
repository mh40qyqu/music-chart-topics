package crawler

import (
	"crypto/tls"
	"time"

	"io/ioutil"
	"log"
	"net/http"
	"sync"
)

//CrawlResponse is the get request response from a website respresented as a struct containing the url and the response body in []byte
type CrawlResponse struct {
	URL          string
	ResponseBody []byte
}

//MakeHTTPRequest executes a single http get request
func MakeHTTPRequest(url string, securityChecks bool) CrawlResponse {
	tr := &http.Transport{}
	if securityChecks == false {
		//Disable security checks for this time -> not a good idea generally!
		tr = &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
	}

	client := &http.Client{Transport: tr, Timeout: 10 * time.Second}
	log.Printf("Requesting %s\n", url)
	req, _ := http.NewRequest("GET", url, nil)
	resp, err := client.Do(req)

	if err != nil {
		log.Fatalln(err.Error())
	}

	cResponse := CrawlResponse{URL: url}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	cResponse.ResponseBody = bodyBytes
	return cResponse
}

//MakeAllHTTPRequests reqquests all given urls in the array, returns an array of crawlresponses
func MakeAllHTTPRequests(urls []string, securityChecks bool, chartspersite int) []CrawlResponse {
	responseBodies := make([]CrawlResponse, len(urls))

	requestChannel := make(chan CrawlResponse)
	var wg sync.WaitGroup

	for i := range responseBodies {
		wg.Add(1)
		go makeSingleHTTPRequest(urls[i], securityChecks, requestChannel, &wg)
	}

	for i := range responseBodies {
		responseBodies[i] = <-requestChannel
	}

	wg.Wait()

	return responseBodies
}

//makeSingleHTTPRequest executes a single http get request, using a channel, this method is used in MakeAllHTTPRequests
func makeSingleHTTPRequest(url string, securityChecks bool, channel chan<- CrawlResponse, wg *sync.WaitGroup) {
	defer wg.Done()
	tr := &http.Transport{}
	if securityChecks == false {
		//Disable security checks for this time -> not a good idea generally!
		tr = &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
	}

	client := &http.Client{Transport: tr, Timeout: 7 * time.Second}
	log.Printf("Requesting %s\n", url)
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; "+
		"Windows NT 5.2; .NET CLR 1.0.3705;)")

	resp, err := client.Do(req)

	if err != nil {
		log.Println("Error when making single http request: ", err.Error())
		channel <- CrawlResponse{}
		return
	}

	cResponse := CrawlResponse{URL: url}
	bodyBytes, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Println("Received nil response body!")
		cResponse.ResponseBody = nil
		channel <- cResponse
		return
	}
	cResponse.ResponseBody = bodyBytes
	channel <- cResponse
}

//MakeGetRequest requests a given url and returning the response body as byte array
func MakeGetRequest(url string) []byte {
	// Create a new request using http
	log.Printf("Requesting for Retrieve Lyrics %s", url)
	req, err := http.NewRequest("GET", url, nil)

	req.Header.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; "+
		"Windows NT 5.2; .NET CLR 1.0.3705;)")

	// Send req using http Client
	client := &http.Client{Timeout: 7 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR] -", err)
		return nil
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error occurred when reading get response for genius api!: ", err.Error())
		return nil
	}
	return body
}
