package crawler

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"music-chart-topics/src/util"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

//RESPONSES FROM MOURITS API
type mouritsResponse struct {
	Success bool          `json:"success"`
	Result  mouritsresult `json:"result"`
	Artist  string        `json:"artist"`
	Song    string        `json:"song"`
	Error   string        `json:"error"`
}

type mouritsresult struct {
	Lyrics string        `json:"lyrics"`
	Source mouritssource `json:"source"`
}

type mouritssource struct {
	Name     string `json:"name"`
	Homepage string `json:"homepage"`
	URL      string `json:"url"`
}

// MouritsAPIURL the url of the mourits api
const MouritsAPIURL = "https://mourits.xyz:2096/"

//CreateLyricsJSONFromTSVData read in the chart tsv data, crawl the lyrics for every song and save the results in a json file
func CreateLyricsJSONFromTSVData(charts []util.ChartTSVEntry, chartspersite int, year int) error {
	lyricsFileName := "../data/lyrics/ChartLyrics_" + strconv.Itoa(year) + ".json"

	startIndex, endIndex := -1, -1

	//only parse charts of the given year -> create slice
	for i, chart := range charts {

		//only do this one time
		if (chart.Year) == year && startIndex == -1 {
			startIndex = i
		}

		if chart.Year != year && startIndex != -1 {
			endIndex = i
			break
		}
	}

	if (endIndex - startIndex) == chartspersite {
		charts = charts[startIndex:endIndex]
	} else {
		charts = charts[startIndex:(startIndex + chartspersite)]
	}

	data := crawlForLyricsByTSVCharts(charts, chartspersite, year)
	return util.WriteLyricsDataAsJSON(data, lyricsFileName)
}

//crawlForLyricsByTSVCharts crawls for lyrics by requesting the artists/ titles from the data/ChartList.txt
func crawlForLyricsByTSVCharts(charts []util.ChartTSVEntry, chartspersite int, year int) []util.LyricsEntity {

	allLyrics := make([]util.LyricsEntity, chartspersite)
	counter := 0

	for i, chart := range charts {
		//skip entries which does not match the requested year
		if chart.Year != year {
			log.Println("Skipping not matching year! ", i)
			continue
		}

		if len(chart.Title) == 0 || len(chart.Artist) == 0 {
			log.Println("Empty title or artist!")
			continue
		}
		allLyrics[counter] = *crawlForLyric(chart, 0)
		counter++

		//because the mourits api endpoint is rather slow we need to wait a little bit before requesting again
		//time.Sleep(150 * time.Millisecond)
	}
	return allLyrics
}

//ReCrawlEmptyLyrics reads in a json file, searchs for lyrics fields with the value NONE and makes new requests against the mourits api to hopefully find more lyrics
func ReCrawlEmptyLyrics(lyricsDir string) {
	files, err := ioutil.ReadDir(lyricsDir)
	if err != nil {
		log.Fatalln("ReCrawlEmptyLyrics: Error when trying to read directory for json lyrics -> ", err.Error())
	}

	for _, file := range files {

		fileYear := strings.Split(file.Name(), "_")[1]
		fileYear = strings.Split(fileYear, ".")[0]

		//TODO: delete this later
		/*if year, err := strconv.Atoi(fileYear); year < 1990 && err == nil {
			log.Println("Skipping", file.Name())
			continue
		}*/

		log.Println("Processing ", file.Name())
		lyricsEntities := util.ReadLyricEntityFromJSONFile(lyricsDir + file.Name())

		for i, entity := range lyricsEntities {
			if entity.Lyrics == "None" {
				entity = *crawlForLyric(entity.Chart, 0)
				lyricsEntities[i] = entity
			}
		}

		util.WriteLyricsDataAsJSON(lyricsEntities, lyricsDir+file.Name())
	}
}

//crawlForLyric try to retrieve lyrics from a chartTSV entry, the tryCount is used for recursive calling of the function when a request fails, stop after some amount of retries
func crawlForLyric(charttsv util.ChartTSVEntry, tryCount int) *util.LyricsEntity {
	if tryCount > 2 {
		log.Println("Max number of retries for retrieving lyrics reached!")
		return &util.LyricsEntity{
			Chart:     charttsv,
			Lyrics:    "None",
			LyricURL:  "None",
			FullTitle: "None",
		}
	}

	baseURL, err := url.Parse(MouritsAPIURL)
	if err != nil {
		log.Fatalln("Mourits Base API path is an invalid URL!")
	}

	params := url.Values{}
	//artist parameter
	params.Add("a", util.RemoveEnumerationsAndFeature(charttsv.Artist))
	//song parameter
	params.Add("s", util.RemoveEnumerationsAndFeature(charttsv.Title))

	baseURL.RawQuery = params.Encode()

	url := baseURL.String()

	time.Sleep(150 * time.Millisecond)
	resp := MakeGetRequest(url)

	if resp == nil {
		log.Println("Got nil response body! Returning None lyrics")
		return &util.LyricsEntity{
			Chart:     charttsv,
			Lyrics:    "None",
			LyricURL:  "None",
			FullTitle: "None",
		}
	}

	mouritzresp := &mouritsResponse{}
	json.Unmarshal(resp, &mouritzresp)

	if mouritzresp.Success == false {
		return crawlForLyric(charttsv, tryCount+1)
	}

	return &util.LyricsEntity{
		Chart:     charttsv,
		Lyrics:    mouritzresp.Result.Lyrics,
		LyricURL:  mouritzresp.Result.Source.URL,
		FullTitle: mouritzresp.Song,
	}
}

//ReadChartdata reads the charts data from the given filename
func ReadChartdata(filename string) []util.ChartTSVEntry {
	log.Println("Reading in ChartList tsv file...")
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	doc, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalln(err.Error())
	}

	splittedDocument := strings.Split(string(doc), "\n")

	tsvcharts := make([]util.ChartTSVEntry, len(splittedDocument))

	//counter of valid results -> create slice from this
	counter := 0
	for i, line := range splittedDocument {
		//skip tsv head which is -> YEAR CHARTPOS CHARTARTIST CHARTTITLE
		if i == 0 {
			continue
		} else {
			if len(line) == 0 {
				continue
			}

			splittedLine := strings.Split(line, "\t")
			tsvcharts[i-1].Year, err = strconv.Atoi(splittedLine[0])
			if err != nil {
				log.Fatalln("Something went wrong when trying to convert the chart year from tsv string into an integer! ", splittedLine)
			}

			tsvcharts[i-1].ChartPosition, err = strconv.Atoi(splittedLine[1])
			if err != nil {
				log.Fatalln("Something went wrong when trying to convert the chart position from tsv string into an integer! ", splittedLine)
			}
			tsvcharts[i-1].Artist = splittedLine[2]
			tsvcharts[i-1].Title = splittedLine[3]

			counter++
		}
	}
	return tsvcharts
}
