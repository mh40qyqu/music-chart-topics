package crawler

import (
	"log"
	"music-chart-topics/src/util"
	"os"
	"sort"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

// allYearsCharts is a struct containing all 40 charts from one year
type allYearsCharts struct {
	//year of chart placing
	year int
	//list of 40 entries of charts from this year
	chartyearEntry []chartEntry
}

type chartEntry struct {
	//number of chart placing
	chartnumber int
	//artist and song title as one string
	artist string
	title  string
}

const chartURL = "https://www.top40charts.net/"

//CreateChartMusicList creates the chart music list on the given filename position
func CreateChartMusicList(filename string, chartsPerYear, startYear, endYear int) {

	chartURLS := generateChartUrls(startYear, endYear)

	allResponses := MakeAllHTTPRequests(chartURLS, false, chartsPerYear)
	cleanData := clearData(allResponses, chartsPerYear)

	sortCleanedData(cleanData)

	util.CreateFile(filename)
	writeFile(filename, cleanData)
}

func sortCleanedData(clanedData []allYearsCharts) {
	//sorting slice by chart year
	log.Println("Sorting slice by chart year...")
	sort.Slice(clanedData, func(i, j int) bool { return clanedData[i].year < clanedData[j].year })
}

func writeFile(path string, cleanData []allYearsCharts) {
	// Open file using READ & WRITE permission.
	var file, err = os.OpenFile(path, os.O_RDWR, 0644)
	if util.IsError(err) {
		return
	}
	defer file.Close()
	//write first line as tsv header line
	file.WriteString("chartYear" + "\t" + "chartNumber" + "\t" + "chartArtist" + "\t" + "chartTitle\n")

	for _, value := range cleanData {
		for _, entry := range value.chartyearEntry {
			cleanUpString(&entry.artist)
			cleanUpString(&entry.title)

			//in case of containing the end of file copyright claims, dont save it to the file
			if strings.Contains(entry.artist, "Copyright") || strings.Contains(entry.title, "Copyright") {
				continue

				//dont save empty entries
			} else if len(entry.artist) == 0 || len(entry.title) == 0 {
				continue
			}

			file.WriteString(strconv.Itoa(value.year) + "\t" + strconv.Itoa(entry.chartnumber) + "\t" + entry.artist + "\t" + entry.title + "\n")
		}
	}
	log.Println("File Writing done Successfully.")
}

func cleanUpString(str *string) {
	*str = strings.ReplaceAll(*str, "''", "\"")
	*str = strings.ReplaceAll(*str, "\n", "")
	*str = strings.ReplaceAll(*str, "\t", "")
	*str = strings.ReplaceAll(*str, " '", "'")
	*str = strings.ReplaceAll(*str, "   ", " ")
	*str = strings.ReplaceAll(*str, "  ", " ")
	*str = strings.TrimSpace(*str)
}

func clearData(cResponse []CrawlResponse, chartsPerYear int) []allYearsCharts {
	cleanedData := make([]allYearsCharts, len(cResponse))

	for i, value := range cResponse {
		//get year from the url string
		year, _ := strconv.Atoi(strings.TrimSuffix(strings.Split(value.URL, "/")[3], ".htm"))
		cleanedData[i].year = year

		htmlTokens := html.NewTokenizer(strings.NewReader(string(value.ResponseBody)))
		htmlTokens.AllowCDATA(false)

		idCounter := 0
		isArtist := true
		chartsFromYear := make([]chartEntry, chartsPerYear)
	loop:
		for {
			tt := htmlTokens.Next()
			switch tt {
			case html.ErrorToken:
				break loop

			case html.StartTagToken:
				t := htmlTokens.Token()
				if t.Data == "p" {
					text := getAllTextTokens(htmlTokens)
					if len(text) == 0 {
						break
					}

					if isArtist == true && idCounter < chartsPerYear {
						chartsFromYear[idCounter].artist = text
						isArtist = false

					} else if isArtist == false && idCounter < chartsPerYear {
						chartsFromYear[idCounter].title = text
						chartsFromYear[idCounter].chartnumber = idCounter + 1
						//reset isArtist flag
						isArtist = true
						idCounter++
					}
				}
			}
		}
		cleanedData[i].chartyearEntry = chartsFromYear
	}
	return cleanedData
}

//getAllTextTokens retrieves all normal text tokens inside a <p> paragraph
func getAllTextTokens(htmlTokens *html.Tokenizer) string {
	text := ""
	for {
		tt := htmlTokens.Next()
		if tt == html.TextToken {
			TxtContent := strings.TrimSpace(html.UnescapeString(string(htmlTokens.Text())))
			if len(TxtContent) > 0 {
				text += " " + TxtContent
			}
		} else if tt == html.ErrorToken {
			break
		} else if tt == html.EndTagToken {
			if htmlTokens.Token().Data == "p" {
				break
			}
		}
	}
	text = strings.ReplaceAll(text, "\n", " ")
	return text
}

func generateChartUrls(startYear int, endYear int) []string {
	allChartUrls := make([]string, endYear-startYear+1)
	for i := startYear; i <= endYear; i++ {
		it := i - startYear
		allChartUrls[it] = chartURL + strconv.Itoa(i) + ".htm"
	}
	return allChartUrls
}
