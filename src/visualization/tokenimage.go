package visualization

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"log"
	"math"
	"music-chart-topics/src/util"
	"os"
	"strconv"
	"strings"
	"sync"

	"gopkg.in/jdkato/prose.v2"
)

type wordToken struct {
	word string
	tag  string
}

var (
	nounColor      = color.RGBA{255, 0, 0, 255}
	verbColor      = color.RGBA{0, 255, 0, 255}
	attributeColor = color.RGBA{0, 0, 255, 255}
)

//GenerateAllVisualizations reads in all lyricsentity files from a directory and creates a token visualization for every file
func GenerateAllVisualizations(fileDir string) {
	files := util.GetAllFilesFromDirectory(fileDir)

	wg := sync.WaitGroup{}

	for _, file := range files {
		wg.Add(1)
		go GenerateVisualizationFromJSONFile(fileDir+file.Name(), &wg)
	}
	wg.Wait()
}

//GenerateVisualizationFromJSONFile reads in an uncleaned lyrics file, tokenizes it and creates an image from the tokens
//this function operates year-wise, so we want to create an image of all words per chart-year
func GenerateVisualizationFromJSONFile(filename string, wg *sync.WaitGroup) {
	fmt.Println("Generate visualization for: ", filename)
	rawLyrics := util.ReadLyricEntityFromJSONFile(filename)

	allTokens := make([][]wordToken, len(rawLyrics))

	for i, elem := range rawLyrics {
		allTokens[i] = make([]wordToken, 0)
		allTokens[i] = append(allTokens[i], getTokenList(elem.Lyrics)...)
	}

	//fmt.Println(allTokens)

	img := createImageFromTaggedTokens(allTokens)

	imgFileName := strings.Split(filename, "/")[2]
	imgFileName = strings.TrimSuffix(imgFileName, "json")
	imgFileName = "images/tokenvisualization/" + imgFileName + "png"
	saveImageToFile(imgFileName, img)
	wg.Done()
}

func getTokenList(lyrics string) []wordToken {
	doc, err := prose.NewDocument(lyrics,
		prose.WithTokenization(true),
		prose.WithTagging(true),
		prose.WithExtraction(false),
		prose.WithSegmentation(false))

	if err != nil {
		log.Fatalln("TokenizeLyrics: Error when trying to create document for tokenizing lyrics! -> ", err.Error())
		return []wordToken{}
	}

	tokens := make([]wordToken, len(doc.Tokens()))

	for i, token := range doc.Tokens() {
		tokens[i].word = token.Text
		tokens[i].tag = token.Tag
	}
	return tokens
}

func createImageFromTaggedTokens(tokenList [][]wordToken) *image.RGBA {

	longestRow := findLongestTokenRow(tokenList)

	img := image.NewRGBA(image.Rect(0, 0, longestRow, len(tokenList)))

	//initialize image with all white pixel
	bounds := img.Rect
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {

			if x >= len(tokenList[y]) {
				img.SetRGBA(x, y, color.RGBA{
					R: 255,
					G: 255,
					B: 255,
					A: 255,
				})
				continue
			}

			switch {
			case strings.HasPrefix(tokenList[y][x].tag, "N") == true:
				img.Set(x, y, nounColor)

			case strings.HasPrefix(tokenList[y][x].tag, "V") == true:
				img.Set(x, y, verbColor)

			case strings.HasPrefix(tokenList[y][x].tag, "J") == true:
				img.Set(x, y, attributeColor)
			default:
				img.SetRGBA(x, y, color.RGBA{
					R: 255,
					G: 255,
					B: 255,
					A: 255,
				})
			}
		}
	}
	return img
}

func findLongestTokenRow(wordTokens [][]wordToken) int {
	longestRow := 0
	for _, elem := range wordTokens {
		if len(elem) > longestRow {
			longestRow = len(elem)
		}
	}
	return longestRow
}

func saveImageToFile(filename string, img *image.RGBA) bool {
	outputFile, err := os.Create(filename)

	defer outputFile.Close()

	if util.IsError(err) {
		return false
	}
	png.Encode(outputFile, img)
	return true
}

func readImagesFromFile(folderDir string, startYear int, endYear int) []*image.RGBA {
	files := util.GetAllFilesFromDirectory(folderDir)

	tokenImages := make([]*image.RGBA, endYear-startYear+1)
	for i, imgFile := range files {

		//extract year from fileNames, if not in intervall -> skip
		yearString := strings.Split(imgFile.Name(), "_")[1]
		yearString = strings.TrimSuffix(yearString, ".png")

		if year, _ := strconv.Atoi(yearString); year < startYear || year > endYear {
			continue
		}

		fmt.Println("Read in: ", imgFile.Name())

		osFile, err := os.Open(folderDir + imgFile.Name())

		if util.IsError(err) == true {
			return []*image.RGBA{}
		}

		img, err := png.Decode(osFile)
		imgRGBA, ok := img.(*image.RGBA)
		if ok == false {
			fmt.Println("cannot convert image.Image to image.RGBA! Returning empty slice...")
			return []*image.RGBA{}
		}
		tokenImages[i%10] = imgRGBA
	}
	return tokenImages
}

//CreateAverageImageForEveryDecade creates an average image for all token images per decade
func CreateAverageImageForEveryDecade(dataDir string) {
	imgArray := make([]*image.RGBA, 10)
	for i := 0; i < 7; i++ {
		startYear := 1950 + (10 * i)
		endYear := 1959 + (10 * i)

		fmt.Println("Create average image for:", startYear, "-", endYear)
		imgArray = readImagesFromFile(dataDir, startYear, endYear)
		avgImg := createAverageImage(imgArray)
		saveImageToFile("images/averagetokenimages/AverageTokens_"+strconv.Itoa(startYear)+".png", avgImg)
	}
}

//createAverageImage a function which gets an array of rgba images passed and returns an average calculated rgba image with the size of the largest image in the array
func createAverageImage(imageArr []*image.RGBA) *image.RGBA {
	maxWidth, maxHeight := 0, 0
	// first we need to find the maximum of width and length of all images in the slice
	for _, elem := range imageArr {
		if maxWidth < elem.Bounds().Max.X {
			maxWidth = elem.Bounds().Max.X
		}

		if maxHeight < elem.Bounds().Max.Y {
			maxHeight = elem.Bounds().Max.Y
		}
	}

	//create new image from maximum width and height
	avgImg := image.NewRGBA(image.Rect(0, 0, maxWidth, maxHeight))
	rAvg, gAvg, bAvg := 0, 0, 0
	for y := 0; y < maxHeight; y++ {
		for x := 0; x < maxWidth; x++ {
			rAvg = 0
			gAvg = 0
			bAvg = 0
			for _, img := range imageArr {
				//check if we try to access out of bounds image
				if y >= img.Bounds().Max.Y || x >= img.Bounds().Max.X {
					rAvg += 255
					gAvg += 255
					bAvg += 255
					continue
				}

				color := img.At(x, y).(color.RGBA)
				rAvg += int(color.R)
				gAvg += int(color.G)
				bAvg += int(color.B)
			}

			rAvg = rAvg / len(imageArr)
			gAvg = gAvg / len(imageArr)
			bAvg = bAvg / len(imageArr)

			avgImg.Set(x, y, color.RGBA{uint8(rAvg), uint8(gAvg), uint8(bAvg), 255})
		}
	}
	return avgImg
}

//CalcEuclideanDistanceBetweenTokenImages is a funtion to calculate the 2-norm / euclidean distance between two rgba images
func CalcEuclideanDistanceBetweenTokenImages(image1 *image.RGBA, image2 *image.RGBA) float64 {
	maxWidth := 0
	maxHeight := 0

	if image1.Bounds().Max.X > image2.Bounds().Max.X {
		maxWidth = image1.Bounds().Max.X
	} else {
		maxWidth = image2.Bounds().Max.X
	}

	if image1.Bounds().Max.Y > image2.Bounds().Max.Y {
		maxHeight = image1.Bounds().Max.Y
	} else {
		maxHeight = image2.Bounds().Max.Y
	}

	colorImg1 := color.RGBA{}
	colorImg2 := color.RGBA{}
	white := color.RGBA{255, 255, 255, 255}

	var euclideanDistance float64 = 0.0

	for y := 0; y < maxHeight; y++ {
		for x := 0; x < maxWidth; x++ {

			if y >= image1.Bounds().Max.Y || x >= image1.Bounds().Max.X {
				colorImg1 = white
			} else {
				colorImg1 = image1.At(x, y).(color.RGBA)
			}

			if y >= image2.Bounds().Max.Y || x >= image2.Bounds().Max.X {
				colorImg2 = white
			} else {
				colorImg2 = image2.At(x, y).(color.RGBA)
			}

			//each channel of rgba represents a dimension in the distance calculation
			//the positional dimensions can be omitted because we normalize all used images
			euclideanDistance += math.Pow(float64(colorImg1.R-colorImg2.R), 2.0) + math.Pow(float64(colorImg1.G-colorImg2.G), 2.0) + math.Pow(float64(colorImg1.B-colorImg2.B), 2.0)
		}
	}
	return math.Sqrt(euclideanDistance)
}
