package visualization

import (
	"encoding/json"
	"fmt"
	"image/png"
	"io/ioutil"
	"log"
	"music-chart-topics/src/nlp"
	"music-chart-topics/src/util"
	"os"
	"sort"
	"strings"

	"github.com/psykhi/wordclouds"
	"github.com/wcharczuk/go-chart"
	"github.com/wcharczuk/go-chart/drawing"
)

const (
	graphDir      = "images/graphs/"
	topicGraphDir = "images/topics/"
)

var (
	lrd []nlp.LexicalRichnessData
)

type selectedTopic struct {
	Topic string
	Words []string
}

//CreateAllGraphs renders all graphs we can currently produce
func CreateAllGraphs() {

	//important!
	lrd = nlp.ReadLexicalRichnessData()

	yearArray := make([]float64, len(lrd))

	for i := range lrd {
		yearArray[i] = float64(lrd[i].Year)
	}

	createAverageTermLengthGraph(yearArray)
	createGraphDifferenceAvgTermLength(yearArray)
	createLexicalRichnessGraph(yearArray)
	createTermCountGraph(yearArray)
	createTermMapSizeGraph(yearArray)
	createTermsOccuringOnceGraph(yearArray)
	createTotalTermMapGrowthGraph(yearArray)
	createTermsUsedOnceRatioGraph(yearArray)

	CreateCountTermGraphFromTermList([]string{"love", "money", "peace", "war", "fear", "woman", "man", "hate", "girl", "weapon", "gun", "thinkin'"})

	CreateMovingWindowTokenTypeRatioDistributionGraph()
}

//CreateCountTermGraphFromTermList creates multiple CountTerm Graphs representing the number of occurrences for each term in each year
func CreateCountTermGraphFromTermList(termList []string) {
	for _, term := range termList {
		CreateCountTermGraph(term)
	}
}

//CreateCountTermGraph creates a graph representing the count of the given term
func CreateCountTermGraph(term string) {
	termCountArray := make([]float64, nlp.EndYear-nlp.StartYear+1)
	yearArray := make([]float64, nlp.EndYear-nlp.StartYear+1)
	for i := nlp.StartYear; i <= nlp.EndYear; i++ {
		termCountArray[i-nlp.StartYear] = float64(nlp.GetCountOfTerm(term, i))
		yearArray[i-nlp.StartYear] = float64(i)
	}

	graph := ProduceGraphBones("Term Count Graph: "+term, "term count ", true, yearArray, termCountArray, true, "years", "count of term occurence")

	pngFile, err := os.Create(graphDir + "/term_count_graphs/" + "term_count_graph_" + term + ".png")
	if err != nil {
		log.Fatalln("CreateTermGraph create: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("CreateTermGraph render: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("CreateTermGraph close: ", err)
	}

}

//createAverageTermLengthGraph creates a graphical representation of the average term length of all years
func createAverageTermLengthGraph(yearArray []float64) {
	yValues := make([]float64, len(lrd))

	for i := range lrd {
		yValues[i] = float64(lrd[i].AverageTermLengthWithSepcialChars)
	}

	graph := ProduceGraphBones("Average Term Length Graph", "average term length value", true, yearArray, yValues, false, "years", "average term length")

	pngFile, err := os.Create(graphDir + "average_term_length.png")
	if err != nil {
		log.Fatalln("createAverageTermLengthGraph: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("createAverageTermLengthGraph: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("createAverageTermLengthGraph: ", err)
	}
}

//createGraphDifferenceAvgTermLength produces a graph represneting the difference of the average term length with and without special character removal
func createGraphDifferenceAvgTermLength(yearArray []float64) {
	yValues := make([]float64, len(lrd))

	for i := range lrd {
		yValues[i] = float64(lrd[i].AverageTermLengthWithSepcialChars) - float64(lrd[i].AverageTermLength)
	}

	graph := ProduceGraphBones("Average Term Length Difference Graph", "difference", true, yearArray, yValues, false, "years", "average term length difference")

	pngFile, err := os.Create(graphDir + "average_term_length_difference.png")
	if err != nil {
		log.Fatalln("createGraphDifferenceAvgTermLength: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("createGraphDifferenceAvgTermLength: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("createGraphDifferenceAvgTermLength: ", err)
	}
}

//createTermCountGraph creates a graphical representation of the term count per year
func createTermCountGraph(yearArray []float64) {
	yValues := make([]float64, len(lrd))

	for i := range lrd {
		yValues[i] = float64(lrd[i].TermCount)
	}

	graph := ProduceGraphBones("Term Count Graph", "number of terms", true, yearArray, yValues, true, "years", "term count per year")

	pngFile, err := os.Create(graphDir + "term_count.png")
	if err != nil {
		log.Fatalln("createTermCountGraph: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("createTermCountGraph: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("createTermCountGraph: ", err)
	}
}

//createTermMapSizeGraph creates a graphical representation of the term map size, representing the count of unique word types
func createTermMapSizeGraph(yearArray []float64) {
	yValues := make([]float64, len(lrd))

	for i := range lrd {
		yValues[i] = float64(lrd[i].TermMapSize)
	}

	graph := ProduceGraphBones("Types Graph", "number of types", true, yearArray, yValues, true, "year", "types")

	pngFile, err := os.Create(graphDir + "term_map_size.png")
	if err != nil {
		log.Fatalln("createTermMapSizeGraph: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("createTermMapSizeGraph: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("createTermMapSizeGraph: ", err)
	}
}

//createLexicalRichnessGraph creates a graphical representation of the lexical richness
func createLexicalRichnessGraph(yearArray []float64) {
	yValues := make([]float64, len(lrd))

	for i := range lrd {
		yValues[i] = float64(lrd[i].LexicalRichness)
	}

	graph := ProduceGraphBones("Lexical Richness Graph - Token Type Ratio (Herdan 1960)", "lexical richness value", true, yearArray, yValues, false, "years", "lexical richness value")

	pngFile, err := os.Create(graphDir + "lexical_richness.png")
	if err != nil {
		log.Fatalln("createLexicalRichnessGraph: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("createLexicalRichnessGraph: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("createLexicalRichnessGraph: ", err)
	}
}

//createTotalTermMapGrowthGraph creates a graphical representation of the lexical richness
func createTotalTermMapGrowthGraph(yearArray []float64) {
	yValues := make([]float64, len(lrd))

	oldValue := 0.0
	for i := range lrd {
		yValues[i] = float64(lrd[i].TotalTermMapSize) - oldValue
		oldValue = float64(lrd[i].TotalTermMapSize)
	}

	graph := ProduceGraphBones("Vocabulary Growth Graph", "types", false, yearArray, yValues, true, "years", "type count")

	pngFile, err := os.Create(graphDir + "term_map_growth.png")
	if err != nil {
		log.Fatalln("createTotalTermMapGrowthGraph: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("createTotalTermMapGrowthGraph: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("createTotalTermMapGrowthGraph: ", err)
	}
}

//createTermsOccuringOnceGraph creates a graphical representation of the lexical richness
func createTermsOccuringOnceGraph(yearArray []float64) {
	yValues := make([]float64, len(lrd))

	for i := range lrd {
		yValues[i] = float64(lrd[i].TermsOccuringOnce)
	}

	graph := ProduceGraphBones("Terms Occuring Once Graph", "number of terms occuring once", true, yearArray, yValues, true, "years", "number of terms occuring once")

	pngFile, err := os.Create(graphDir + "terms_occuring_once.png")
	if err != nil {
		log.Fatalln("createTermsOccuringOnceGraph: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("createTermsOccuringOnceGraph: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("createTermsOccuringOnceGraph: ", err)
	}
}

//CreateMovingWindowTokenTypeRatioDistributionGraph creates a graph representing the MWTTRD of all decades
func CreateMovingWindowTokenTypeRatioDistributionGraph() {
	//the window size parameter for mwttrd
	var L = 100
	//array representing 7 decades of mwttrd
	var mwttrdOfDecade = make([][]float64, 7)

	decadeCounter := -1

	for i := nlp.StartYear; i <= nlp.EndYear; i++ {
		if i%10 == 0 {
			decadeCounter++
			//reached a new decade
			mwttrdOfDecade[decadeCounter] = nlp.CalcMWTTRD(i, L)
			continue
		}
		mwttrdArr := nlp.CalcMWTTRD(i, L)

		for j := 0; j < len(mwttrdArr); j++ {
			mwttrdOfDecade[decadeCounter][j] += mwttrdArr[j]
		}
	}

	for i := range mwttrdOfDecade {
		//calc average
		for j := range mwttrdOfDecade[i] {
			mwttrdOfDecade[i][j] /= float64(10)
		}
	}

	xValues := make([]float64, L)
	for i := range xValues {
		xValues[i] = float64(i)
	}

	var series []chart.Series
	decadeSeries := make([]chart.ContinuousSeries, len(mwttrdOfDecade))

	decadeColors := []drawing.Color{
		{
			R: 0,
			G: 0,
			B: 0,
			A: 255,
		},
		{
			R: 255,
			G: 0,
			B: 0,
			A: 255,
		},
		{
			R: 0,
			G: 255,
			B: 0,
			A: 255,
		},
		{
			R: 0,
			G: 0,
			B: 255,
			A: 255,
		},
		{
			R: 255,
			G: 255,
			B: 0,
			A: 255,
		},
		{
			R: 0,
			G: 255,
			B: 255,
			A: 255,
		},
		{
			R: 255,
			G: 0,
			B: 255,
			A: 255,
		},
	}

	for decade := range mwttrdOfDecade {
		decadeSeries[decade] = chart.ContinuousSeries{
			Name:            fmt.Sprintf("MWTTRD %d", nlp.StartYear+(decade*10)),
			XValues:         xValues,
			YValues:         mwttrdOfDecade[decade],
			XValueFormatter: chart.IntValueFormatter,
			Style: chart.Style{
				StrokeColor: decadeColors[decade],
			},
		}
		series = append(series, decadeSeries[decade])
	}

	graph := chart.Chart{
		Title: "Moving Window Token Type Ratio Distribution Of Decades 1950-2010",
		Background: chart.Style{
			Hidden: false,
			Padding: chart.Box{
				Top:    120,
				Left:   10,
				Right:  10,
				Bottom: 10,
				IsSet:  false,
			},
		},
		YAxis: chart.YAxis{
			Name: "percentage",
		},
		XAxis: chart.XAxis{
			Name: "number of types in 100 tokens",
		},
		Series: series,
	}

	graph.Elements = []chart.Renderable{
		chart.LegendThin(&graph),
	}

	pngFile, err := os.Create(graphDir + "mwttrd_decades.png")
	if err != nil {
		log.Fatalln("CreateMovingWindowTokenTypeRatioDistributionGraph: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("CreateMovingWindowTokenTypeRatioDistributionGraph: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("CreateMovingWindowTokenTypeRatioDistributionGraph: ", err)
	}
}

func createTermsUsedOnceRatioGraph(yearArray []float64) {
	yValues := make([]float64, len(lrd))

	yValues2 := make([]float64, len(lrd))
	for i := range yValues2 {
		yValues2[i] = 0.5
	}

	for i := range lrd {
		yValues[i] = float64(lrd[i].RatioTermsOccuringOnce)
	}

	graph := ProduceGraphBones("Types Occurring Once Ratio Graph", "relative value of types used once", true, yearArray, yValues, false, "years", "ratio of types used once")

	series := chart.ContinuousSeries{
		Name:            "50% mark",
		XValues:         yearArray,
		YValues:         yValues2,
		XValueFormatter: chart.IntValueFormatter,
	}
	graph.Series = append(graph.Series, series)

	pngFile, err := os.Create(graphDir + "terms_used_once_ratio.png")
	if err != nil {
		log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
	}
}

//ProduceGraphBones returns a pointer to a graph with the desired properties
func ProduceGraphBones(title string, graph1Name string, useRegression bool, yearValues []float64, Yvalues []float64, convertYFloatsToInt bool, XAxisName, YAxisName string) *chart.Chart {
	var series []chart.Series
	mainseries := chart.ContinuousSeries{
		Name:            graph1Name,
		XValues:         yearValues,
		YValues:         Yvalues,
		XValueFormatter: chart.IntValueFormatter,
	}

	if convertYFloatsToInt {
		mainseries.YValueFormatter = chart.IntValueFormatter
	}

	series = append(series, mainseries)

	if useRegression {
		linRegSeries := &chart.LinearRegressionSeries{
			Name: "regression",
			Style: chart.Style{
				Hidden: false,
			},
			InnerSeries: mainseries,
		}
		series = append(series, linRegSeries)
	}

	graph := chart.Chart{
		Title: title,
		Background: chart.Style{
			Hidden: false,
			Padding: chart.Box{
				Top:    120,
				Left:   10,
				Right:  10,
				Bottom: 10,
				IsSet:  false,
			},
		},
		XAxis: chart.XAxis{
			Name: XAxisName,
		},
		YAxis: chart.YAxis{
			Name: YAxisName,
		},
		Series: series,
	}

	graph.Elements = []chart.Renderable{
		chart.LegendThin(&graph),
	}
	return &graph
}

func printGraphWithMultipleSeries(title string, xAxisName string, yAxisName string, filename string, series []chart.Series) {
	graph := chart.Chart{
		Title: title,
		Background: chart.Style{
			Hidden: false,
			Padding: chart.Box{
				Top:    120,
				Left:   10,
				Right:  10,
				Bottom: 10,
				IsSet:  false,
			},
		},
		XAxis: chart.XAxis{
			Name: xAxisName,
		},
		YAxis: chart.YAxis{
			Name: yAxisName,
		},
		Series: series,
	}

	graph.Elements = []chart.Renderable{
		chart.LegendThin(&graph),
	}

	pngFile, err := os.Create(topicGraphDir + filename)
	if err != nil {
		log.Fatalln("topics over decades: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("topics over decades: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("topics over decades: ", err)
	}

}

//GenerateBadWordsGraph generates a graph of bad words counter, from startYear to endYear
func GenerateBadWordsGraph(startYear int, endYear int, filename string) {
	lyricsEntities := util.ReadCleanedLyricsFromYearToYear(startYear, endYear)
	badWordsCounterMap := generateBadWordsMap(lyricsEntities, startYear)
	years := make([]float64, 0, len(badWordsCounterMap))
	for year := range badWordsCounterMap {
		years = append(years, float64(year))
	}
	sort.Float64s(years)

	counterValues := make([]float64, len(years))
	for i, year := range years {
		counterValues[i] = float64(badWordsCounterMap[int(year)])
	}
	graph := ProduceGraphBones("Bad Words over the years", "number of bad words", true, years, counterValues, true, "Years", "Number of bad words")

	pngFile, err := os.Create(graphDir + filename)
	if err != nil {
		log.Fatalln("bad words: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("bad words: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("bad words: ", err)
	}

}

func generateBadWordsMap(lyricsEntities [][]util.LyricsEntity, startyear int) map[int]int {
	badWords := util.ReadInBadWords()
	badWordsMap := make(map[int]int, len(lyricsEntities))

	for i, entities := range lyricsEntities {
		counter := 0
		year := startyear + i
		for _, song := range entities {
			for _, token := range util.GetTokenList(song.Lyrics) {
				if index := sort.SearchStrings(badWords, token); index < len(badWords) {
					if token == badWords[index] {
						counter++
						//fmt.Println(token)
					}
				}
			}
		}
		badWordsMap[year] = counter
	}
	return badWordsMap
}

//GenerateTopicWordDistributionGraph prints a graph over topics in the years
func GenerateTopicWordDistributionGraph(filename string) {
	topicMap := make(map[int][]nlp.TopicStruct, 7)
	selectedTopics := getTopicCategories()
	var series []chart.Series
	for year := 1950; year < 2020; year = year + 10 {
		topicMap[year] = getTopicsFromDecade(year)
	}
	yearValues := make([]float64, 0, len(topicMap))
	for key := range topicMap {
		yearValues = append(yearValues, float64(key))
	}
	sort.Float64s(yearValues)
	for _, selectedTopic := range selectedTopics {
		YValues := CountSelectedTopicInYears(selectedTopic, topicMap)
		newseries := chart.ContinuousSeries{
			Name:            selectedTopic.Topic,
			XValues:         yearValues,
			YValues:         YValues,
			XValueFormatter: chart.IntValueFormatter,
			YValueFormatter: chart.IntValueFormatter,
		}
		series = append(series, newseries)
	}
	printGraphWithMultipleSeries("topics over time", "years", "number of related words", "topics_over_years.png", series)
}

//CountSelectedTopicInYears counts the words of a selected topic in a topic Map
func CountSelectedTopicInYears(sTopic selectedTopic, topicMap map[int][]nlp.TopicStruct) []float64 {
	years := make([]int, 0, len(topicMap))
	for year := range topicMap {
		years = append(years, year)
	}
	sort.Ints(years)
	results := make([]float64, len(years))
	for i, year := range years {
		counter := 0
		for _, selectedWord := range sTopic.Words {
			for _, tStruct := range topicMap[year] {
				for _, tWord := range tStruct.Words {
					if selectedWord == tWord.Word {
						counter++
					}
				}
			}
		}
		results[i] = float64(counter)
	}
	return results
}

func getTopicsFromYear(year int) []nlp.TopicStruct {
	path := "data/topics/topics_by_years/"
	filename := fmt.Sprintf("topics_%d.json", year)
	data, err := ioutil.ReadFile(path + filename)
	var topicsFromYear []nlp.TopicStruct
	if util.IsError(err) {
		log.Fatalln("getTopicsFromYear: Error when trying to read json file! -> ", err.Error())
	}
	if err = json.Unmarshal(data, &topicsFromYear); util.IsError(err) {
		log.Fatalln("getTopicsFromYear: Error when trying to parse json file! -> ", err.Error())
	}
	return topicsFromYear
}

func getTopicsFromDecade(beginYear int) []nlp.TopicStruct {
	path := "data/topics/with_verbs/"
	endYear := beginYear + 9
	filename := fmt.Sprintf("topics_%d-%d.json", beginYear, endYear)
	data, err := ioutil.ReadFile(path + filename)
	var topicsFromYear []nlp.TopicStruct
	if util.IsError(err) {
		log.Fatalln("getTopicsFromYear: Error when trying to read json file! -> ", err.Error())
	}
	if err = json.Unmarshal(data, &topicsFromYear); util.IsError(err) {
		log.Fatalln("getTopicsFromYear: Error when trying to parse json file! -> ", err.Error())
	}
	return topicsFromYear
}

func getTopicCategories() []selectedTopic {
	topicPath := "data/topic_words.json"
	var topicCategories []selectedTopic
	data, err := ioutil.ReadFile(topicPath)
	if util.IsError(err) {
		log.Fatalln("getTopicCategories: Error when trying to read json file! -> ", err.Error())
	}
	if err = json.Unmarshal(data, &topicCategories); util.IsError(err) {
		log.Fatalln("getTopicCategories: Error when trying to parse json file! -> ", err.Error())
	}
	return topicCategories
}

//GenerateSeperateTopicWordDistributionGraph prints a graph over topics in the years
func GenerateSeperateTopicWordDistributionGraph() {
	topicMap := make(map[int][]nlp.TopicStruct, 7)
	selectedTopics := getTopicCategories()
	for year := 1950; year < 2020; year = year + 10 {
		topicMap[year] = getTopicsFromDecade(year)
	}
	yearValues := make([]float64, 0, len(topicMap))
	for key := range topicMap {
		yearValues = append(yearValues, float64(key))
	}
	sort.Float64s(yearValues)
	for _, selectedTopic := range selectedTopics {
		YValues := CountSelectedTopicInYears(selectedTopic, topicMap)
		graph := ProduceGraphBones(selectedTopic.Topic, selectedTopic.Topic+" count", true, yearValues, YValues, true, "years", "number of related words")
		pngFile, err := os.Create(topicGraphDir + "topic_" + selectedTopic.Topic + ".png")
		if err != nil {
			log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
		}

		err = graph.Render(chart.PNG, pngFile)

		if err != nil {
			log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
		}

		if err := pngFile.Close(); err != nil {
			log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
		}
	}
	//printGraphWithMultipleSeries("topics over time", "years", "number of related words", "topics_over_years.png", series)
}

//GenerateTopicWordDistributionGraphByYear prints a graph over topics in the years
func GenerateTopicWordDistributionGraphByYear() {
	topicMap := make(map[int][]nlp.TopicStruct, 7)
	selectedTopics := getTopicCategories()
	for year := 1950; year < 2020; year++ {
		topicMap[year] = getTopicsFromYear(year)
	}
	yearValues := make([]float64, 0, len(topicMap))
	for key := range topicMap {
		yearValues = append(yearValues, float64(key))
	}
	sort.Float64s(yearValues)
	for _, selectedTopic := range selectedTopics {
		YValues := CountSelectedTopicInYears(selectedTopic, topicMap)
		graph := ProduceGraphBones(selectedTopic.Topic, selectedTopic.Topic+" count", true, yearValues, YValues, true, "years", "number of related words")
		pngFile, err := os.Create(graphDir + "topic_by_year_" + selectedTopic.Topic + ".png")
		if err != nil {
			log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
		}

		err = graph.Render(chart.PNG, pngFile)

		if err != nil {
			log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
		}

		if err := pngFile.Close(); err != nil {
			log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
		}
	}
	//printGraphWithMultipleSeries("topics over time", "years", "number of related words", "topics_over_years.png", series)
}

//PrintTopicAllocationGraph prints a graph where the yvalues are the number of topics per year mapped to the specific topic
func PrintTopicAllocationGraph() {
	selectedTopics := getTopicCategories()
	topicMap := make(map[int][]nlp.TopicStruct, 7)
	for year := 1950; year < 2020; year = year + 10 {
		topicMap[year] = getTopicsFromYear(year)
	}
	yearValues := make([]float64, 0, len(topicMap))
	for key := range topicMap {
		yearValues = append(yearValues, float64(key))
	}
	sort.Float64s(yearValues)
	for _, selectedTopic := range selectedTopics {
		//YValues := CountSelectedTopicInYears(selectedTopic, topicMap)
		YValues := getTopicYValues(topicMap, selectedTopics, selectedTopic)
		graph := ProduceGraphBones(selectedTopic.Topic, selectedTopic.Topic+" count", true, yearValues, YValues, true, "years", "number of related topics")
		pngFile, err := os.Create(graphDir + "topic_allocation_" + selectedTopic.Topic + ".png")
		if err != nil {
			log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
		}

		err = graph.Render(chart.PNG, pngFile)

		if err != nil {
			log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
		}

		if err := pngFile.Close(); err != nil {
			log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
		}
	}
}

func getTopicYValues(topicMap map[int][]nlp.TopicStruct, selectedTopics []selectedTopic, sTopic selectedTopic) []float64 {
	years := make([]int, 0, len(topicMap))
	for year := range topicMap {
		years = append(years, year)
	}
	sort.Ints(years)
	resultArray := make([]float64, len(topicMap))
	for i, year := range years {
		counter := 0
		for _, tStruct := range topicMap[year] {
			bestTopic := getBestTopic(selectedTopics, tStruct)
			if bestTopic == sTopic.Topic {
				counter++
			}
		}
		resultArray[i] = float64(counter)
	}
	return resultArray
}

func getBestTopic(selectedTopics []selectedTopic, tStruct nlp.TopicStruct) string {
	var resultTopic string
	bestValue := 0
	for _, sTopic := range selectedTopics {
		counter := 0
		for _, sTopicWord := range sTopic.Words {
			for _, topicWord := range tStruct.Words {
				if topicWord.Word == sTopicWord {
					counter++
				}
			}
		}
		if counter > bestValue {
			resultTopic = sTopic.Topic
			bestValue = counter
		}
	}
	return resultTopic
}

//GenerateWordcloud prints a wordcloud to a file specified in filename
func GenerateWordcloud(topicPath string, startyear int, topicNumber int, filename string) {
	savepath := "images/wordclouds/"
	topics := getTopicsFromDecadeByPath(startyear, topicPath)
	wordMap := make(map[string]int)
	for _, tStruct := range topics {
		if tStruct.Number == topicNumber {
			for _, tWord := range tStruct.Words {
				wordMap[tWord.Word] = int(tWord.Rel * 1000)
			}
		}
	}

	wordcloud := wordclouds.NewWordcloud(
		wordMap,
		wordclouds.FontFile("/home/denis/Workspace/go/src/golang.org/x/image/font/gofont/ttfs/Go-Regular.ttf"),
		wordclouds.Height(1024),
		wordclouds.Width(1024),
	)
	fmt.Println(wordMap)
	img := wordcloud.Draw()

	wcFile, err := os.Create(savepath + filename)
	if err != nil {
		log.Fatalln("generateWordcloud: ", err)
	}
	png.Encode(wcFile, img)
	if err := wcFile.Close(); err != nil {
		log.Fatalln("generateWordcloud: ", err)
	}
}

func getTopicsFromDecadeByPath(beginYear int, topicPath string) []nlp.TopicStruct {
	path := "data/topics/" + topicPath
	endYear := beginYear + 9
	filename := fmt.Sprintf("topics_%d-%d.json", beginYear, endYear)
	data, err := ioutil.ReadFile(path + filename)
	var topicsFromYear []nlp.TopicStruct
	if util.IsError(err) {
		log.Fatalln("getTopicsFromYear: Error when trying to read json file! -> ", err.Error())
	}
	if err = json.Unmarshal(data, &topicsFromYear); util.IsError(err) {
		log.Fatalln("getTopicsFromYear: Error when trying to parse json file! -> ", err.Error())
	}
	return topicsFromYear
}

//GenerateTopicDiversityGraph generates a graph of the topic diversity
func GenerateTopicDiversityGraph() {
	topicMap := make(map[int][]nlp.TopicStruct, 7)
	for year := 1950; year < 2020; year = year + 10 {
		topicMap[year] = getTopicsFromYear(year)
	}
	yearValues := make([]float64, 0, len(topicMap))
	for key := range topicMap {
		yearValues = append(yearValues, float64(key))
	}
	sort.Float64s(yearValues)
	YValues := make([]float64, len(yearValues))
	for i, year := range yearValues {
		var wordslist []string
		for _, tStruct := range topicMap[int(year)] {
			for _, tWord := range tStruct.Words {
				if !stringSliceContains(wordslist, tWord.Word) {
					wordslist = append(wordslist, tWord.Word)
				}
			}
		}
		YValues[i] = float64(len(wordslist))
	}
	graph := ProduceGraphBones("Diversity of Topics", "number of different words", true, yearValues, YValues, true, "years", "number of unique words")
	pngFile, err := os.Create(topicGraphDir + "diversity_of_topics.png")
	if err != nil {
		log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
	}

	err = graph.Render(chart.PNG, pngFile)

	if err != nil {
		log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
	}

	if err := pngFile.Close(); err != nil {
		log.Fatalln("createTermsUsedOnceRatioGraph: ", err)
	}
}

func stringSliceContains(stringslice []string, value string) bool {
	for _, listval := range stringslice {
		if listval == value {
			return true
		}
	}
	return false
}

//GenerateGraphsOfSelectedTopics prints a graph for each selectedTopicName in the slice
func GenerateGraphsOfSelectedTopics(selectedTopicNames []string) {
	topicMap := make(map[int][]nlp.TopicStruct, 7)
	selectedTopics := make([]selectedTopic, 0, len(selectedTopicNames))
	for _, sTopic := range getTopicCategories() {
		if stringSliceContains(selectedTopicNames, sTopic.Topic) {
			selectedTopics = append(selectedTopics, sTopic)
		}
	}
	var series []chart.Series
	for year := 1950; year < 2020; year = year + 10 {
		topicMap[year] = getTopicsFromDecade(year)
	}
	yearValues := make([]float64, 0, len(topicMap))
	for key := range topicMap {
		yearValues = append(yearValues, float64(key))
	}
	sort.Float64s(yearValues)
	for _, selectedTopic := range selectedTopics {
		YValues := CountSelectedTopicInYears(selectedTopic, topicMap)
		fmt.Println()
		newseries := chart.ContinuousSeries{
			Name:            selectedTopic.Topic,
			XValues:         yearValues,
			YValues:         YValues,
			XValueFormatter: chart.IntValueFormatter,
			YValueFormatter: chart.IntValueFormatter,
		}
		series = append(series, newseries)
	}
	printGraphWithMultipleSeries(strings.Join(selectedTopicNames, ",")+" over time", "years", "number of related words", "topics_"+strings.Join(selectedTopicNames, "_")+".png", series)
}
