package util

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"gopkg.in/jdkato/prose.v2"
)

//ChartTSVEntry represents an entry inside the chart tsv file
type ChartTSVEntry struct {
	Year          int
	ChartPosition int
	Artist        string
	Title         string
}

//LyricsEntity an object representing a song with artist and lyrics
type LyricsEntity struct {
	Chart     ChartTSVEntry
	Lyrics    string
	LyricURL  string
	FullTitle string
}

const (
	lyricDir        = "data/lyrics/"
	cleanedLyricDir = "data/cleanedLyrics/"
	badWordsFile    = "data/bad_words.txt"
)

var (
	//ParenthesisRegex as
	ParenthesisRegex = regexp.MustCompile("(\\(.*?\\)|\\{.*?\\}|\\[.*?\\])")
	//SpecialCharRegex as
	SpecialCharRegex = regexp.MustCompile("[^a-zA-z\\s]")
	//SplitRegex as
	SplitRegex = regexp.MustCompile("(\\r|\\n|\\s)+")
	//Regex for removing features
	featureRegex = regexp.MustCompile("(?i)(.*)(feature|ft\\.|ft|feat\\.|feat|featuring|&|&\\.|,|and)(.*)")
	//Regex for removing I'll it'll i'm
	shortFormRegex = regexp.MustCompile("(i?)('|´|`)(ll|m|t)")

	apostropheRegex = regexp.MustCompile("(i?)('|´|`)")
)

/*//CleanLyrics cleans the lyrics from an array of LyricsEntity of a year
func (lyricsEntity *LyricsEntity) CleanLyrics() {
	if len(lyricsEntity.Lyrics) <= 0 || lyricsEntity.Lyrics == "NONE" {
		log.Println("Cannot clean lyrics for: ", lyricsEntity)
		return
	}
	//remove things written in parenthesis
	lyricsEntity.Lyrics = ParenthesisRegex.ReplaceAllLiteralString(lyricsEntity.Lyrics, "")

	//remove special characters -> let the tokenizer do this
	//TODO: we need rules to elimnate something like "I,I,I,I,I" or ooooo-ooooo (refrain stuff)
	lyricsEntity.Lyrics = SpecialCharRegex.ReplaceAllLiteralString(lyricsEntity.Lyrics, " ")

	//remove all apostrophe words
	lyricsEntity.Lyrics = shortFormRegex.ReplaceAllLiteralString(lyricsEntity.Lyrics, "")
	lyricsEntity.Lyrics = apostropheRegex.ReplaceAllLiteralString(lyricsEntity.Lyrics, "")

	//remove all newlines
	lyricsEntity.Lyrics = strings.ReplaceAll(lyricsEntity.Lyrics, "\n", " ")
}*/

//RemoveEnumerationsAndFeature removes any features and enumerations from a string
func RemoveEnumerationsAndFeature(text string) string {
	matches := featureRegex.FindStringSubmatch(text)
	if matches != nil {
		newArtist := strings.TrimSpace(matches[1])
		if len(newArtist) > 0 {
			text = newArtist
		}
	}
	return text
}

//IsError prettify function for errors
func IsError(err error) bool {
	if err != nil {
		log.Fatalf("Error occured: %s", err.Error())
	}
	return (err != nil)
}

//CreateFile creates a new file if no exists, else clears it
func CreateFile(path string) *os.File {
	// check if file exists
	_, err := os.Stat(path)
	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		defer file.Close()
		if IsError(err) {
			return nil
		}
		log.Printf("File Created Successfully %s", path)
		return file
	}

	log.Printf("File already exist, clear content %s", path)
	file, err := os.OpenFile(path, os.O_RDWR, 0644)
	defer file.Close()
	if IsError(err) {
		return nil
	}

	file.Truncate(0)
	file.Seek(0, 0)
	file.Sync()

	return file
}

//RemoveSliceElement removes a string of given position from the slice
// This function changes the order of the lements
func RemoveSliceElement(slice []string, pos int) []string {
	slice[pos] = slice[len(slice)-1]
	slice[len(slice)-1] = ""
	return slice[:len(slice)-1]
}

//GetAllFilesFromDirectory returns a slice of all files contained in a given directory
func GetAllFilesFromDirectory(fileDir string) []os.FileInfo {
	files, err := ioutil.ReadDir(fileDir)
	if IsError(err) == true {
		return []os.FileInfo{}
	}
	return files
}

//WriteLyricsDataAsJSON writes the crawled lyrics in a json file
func WriteLyricsDataAsJSON(lyricsdata []LyricsEntity, filename string) error {
	CreateFile(filename)
	file, _ := json.MarshalIndent(lyricsdata, "", " ")
	return ioutil.WriteFile(filename, file, 0644)
}

//WriteCleanedLyricsDataAsJSON writes the crawled lyrics in a json file
func WriteCleanedLyricsDataAsJSON(lyricsdata []LyricsEntity, year int) error {
	jsonFile := CreateFile(cleanedLyricDir + "cleaned_lyrics_" + strconv.Itoa(year) + ".json")
	if jsonFile != nil {
		file, _ := json.MarshalIndent(lyricsdata, "", " ")
		return ioutil.WriteFile(jsonFile.Name(), file, 0644)
	}
	return fmt.Errorf("WriteCleanedLyricsDataAsJSON: could create file for saving: %d", year)
}

//ReadLyricEntityFromJSONFile takes a json filename as parameter and returns a list of LyricEntity objects of a year
func ReadLyricEntityFromJSONFile(filename string) []LyricsEntity {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalln("ReadLyricEntityFromJSONFile: Error when trying to read json file! -> ", err.Error())
	}

	var lyricEntityArr = make([]LyricsEntity, 40)

	err = json.Unmarshal(data, &lyricEntityArr)
	if err != nil {
		log.Fatalln("ReadLyricEntityFromJSONFile: Error when trying to parse json! -> ", err.Error())
	}

	return lyricEntityArr
}

//ReadLyricsFromYear reads the json file for a specific year and returns a list of all lyrics from this year
func ReadLyricsFromYear(year int) []LyricsEntity {
	filename := lyricDir + "ChartLyrics_" + strconv.Itoa(year) + ".json"
	return ReadLyricEntityFromJSONFile(filename)
}

//ReadLyricsForDecade reads all json files which contain the lyrics of a given decade and returns them as a 2D array of LyricsEntities
func ReadLyricsForDecade(decadeStart int) [][]LyricsEntity {
	if decadeStart%10 != 0 {
		log.Fatalln("DecadeStart parameter needs to be a start of a decade -> f.e. 1950, 1960, ...")
	}
	decadeLyrics := make([][]LyricsEntity, 10)

	for i := decadeStart; i < decadeStart+10; i++ {
		decadeLyrics[i-decadeStart] = ReadLyricsFromYear(i)
	}
	return decadeLyrics
}

//ReadLyricsFromStartYearToEndYear reads all json files which contain the lyrics of a year intervall
func ReadLyricsFromStartYearToEndYear(startYear, endYear int) [][]LyricsEntity {
	if endYear < startYear {
		log.Fatalln("ReadLyricsFromStartYearToEndYear: Endyear should not be less than Startyear!")
	}
	intervallLyrics := make([][]LyricsEntity, endYear-startYear+1)

	for i := startYear; i <= endYear; i++ {
		intervallLyrics[i-startYear] = ReadLyricsFromYear(i)
	}
	return intervallLyrics
}

//ReadCleanedLyricsFromYear reads the json file for a specific year and returns a list of all lyrics from this year
func ReadCleanedLyricsFromYear(year int) []LyricsEntity {
	filename := cleanedLyricDir + "cleaned_lyrics_" + strconv.Itoa(year) + ".json"
	return ReadLyricEntityFromJSONFile(filename)
}

//ReadCleanedLyricsFromYearToYear reads all cleaned json files which contain the lyrics of a year intervall
func ReadCleanedLyricsFromYearToYear(startYear int, endYear int) [][]LyricsEntity {
	if endYear < startYear {
		log.Fatalln("ReadLyricsFromStartYearToEndYear: Endyear should not be less than Startyear!")
	}
	intervallLyrics := make([][]LyricsEntity, endYear-startYear+1)

	for i := startYear; i <= endYear; i++ {
		intervallLyrics[i-startYear] = ReadCleanedLyricsFromYear(i)
	}
	return intervallLyrics
}

//ReadCleanedLyricsFromDecade reads a whole cleaned Deacde into an 2D-LyricsEntityArray
func ReadCleanedLyricsFromDecade(decadeStart int) [][]LyricsEntity {
	if decadeStart%10 != 0 {
		log.Fatalln("DecadeStart parameter needs to be a start of a decade -> f.e. 1950, 1960, ...")
	}
	decadeLyrics := make([][]LyricsEntity, 10)

	for i := decadeStart; i < decadeStart+10; i++ {
		decadeLyrics[i-decadeStart] = ReadCleanedLyricsFromYear(i)
	}
	return decadeLyrics
}

//ReadInStopWordsFromFile reads common stopwords from a file and returns them as a string
func ReadInStopWordsFromFile(filename string) []string {
	file, err := os.Open(filename)

	if IsError(err) == true {
		return nil
	}

	stopWordsByteArr, err := ioutil.ReadAll(file)

	if IsError(err) {
		return nil
	}

	return strings.Split(string(stopWordsByteArr), "\n")
}

//ReadInBadWords returns a list of the bad_words.txt file
func ReadInBadWords() []string {
	file, err := os.Open(badWordsFile)

	if IsError(err) == true {
		return nil
	}

	badWordsByteArr, err := ioutil.ReadAll(file)

	if IsError(err) {
		return nil
	}

	return sort.StringSlice(strings.Split(string(badWordsByteArr), "\n"))
}

//GetTokenList returns a list of tokens from a string
func GetTokenList(lyrics string) []string {
	doc, err := prose.NewDocument(lyrics,
		prose.WithTokenization(true),
		prose.WithTagging(false),
		prose.WithExtraction(false),
		prose.WithSegmentation(false))

	if err != nil {
		log.Fatalln("TokenizeLyrics: Error when trying to create document for tokenizing lyrics! -> ", err.Error())
		return []string{}
	}

	tokens := make([]string, len(doc.Tokens()))

	for i, token := range doc.Tokens() {
		tokens[i] = token.Text
	}
	return tokens
}
