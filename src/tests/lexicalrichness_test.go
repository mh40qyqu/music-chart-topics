package tests

import (
	"fmt"
	"music-chart-topics/src/nlp"
	"os"
	"testing"
)

// we need to switch directory here
func init() {
	os.Chdir("../../")
	fmt.Println(os.Getwd())

}

func TestCreateLexicalRichnessFile(t *testing.T) {
	nlp.CreateLexicalRichnessFile()
}

func TestGetAverageTermLengthAndTermCount(t *testing.T) {
	avgLength, avgLengthWithSpecialChars, count := nlp.GetAverageTermLengthAndTermCount(1950)
	if avgLength <= 0.0 {
		t.Error("Calculated average term length can't be less than 0!")
	}
	fmt.Println(avgLength, avgLengthWithSpecialChars, count)
}

func TestCreateTermMapOfYear(t *testing.T) {
	wordmap := nlp.CreateTermMapOfYear(1969, true)

	if len(wordmap) == 0 {
		t.Error("Could not create wordmap")
	}

	fmt.Println(wordmap)
}

func TestCalcLexicalRichnessOfYear(t *testing.T) {
	lr := nlp.CalcLexicalRichness(2000)

	if lr == 0.0 {
		t.Error("Something went wrong when calculating lexical richness")
	}

	fmt.Println(lr)
}

func TestGetTermMapGrowth(t *testing.T) {
	newWordNumberList := nlp.GetTermMapGrowth()

	if len(newWordNumberList) == 0 || len(newWordNumberList) != 2019-1950 {
		t.Error("Something went wrong when creating a list with new word counts")
	}

	fmt.Println(newWordNumberList)
}

func TestCreateLexicalRichnessDataOfYear(t *testing.T) {
	lrd := nlp.CreateLexicalRichnessDataOfYear(1969)

	if lrd == nil {
		t.Error("Could not create lexical richness data")
	}

	fmt.Println(lrd)
}
func TestReadLexicalRichnessFile(t *testing.T) {
	le := nlp.ReadLexicalRichnessData()

	if len(le) == 0 {
		t.Errorf("Could not read in lexical richness file!")
	}
	for i := range le {
		fmt.Println(le[i])
	}
}

func TestGetCountTerm(t *testing.T) {
	term := "love"
	fmt.Println("Count of term:", term)
	for i := nlp.StartYear; i <= nlp.EndYear; i++ {
		fmt.Println(i, "-", nlp.GetCountOfTerm(term, i))
	}
}

func TestCalcMATTR(t *testing.T) {
	mattrValue := nlp.CalcMATTR(2019, 100)

	fmt.Println(mattrValue)

	if mattrValue <= 0 {
		t.Errorf("Could not calculate MATTR Value!")
	}
}

func TestCalcMWTTR(t *testing.T) {
	mattrValue := nlp.CalcMWTTR(2019, 100)

	fmt.Println(mattrValue)

	if len(mattrValue) <= 0 {
		t.Errorf("Could not calculate MWTTRD Value!")
	}
}

func TestCalcMWTTRD(t *testing.T) {
	mattrValue := nlp.CalcMWTTRD(2019, 100)

	fmt.Println(mattrValue)

	if len(mattrValue) <= 0 {
		t.Errorf("Could not calculate MWTTRD Value!")
	}
}

func TestCalcRatioTermsOccuringOnce(t *testing.T) {
	ratio := nlp.CalcRatioOfOneTimeUsedTermsOfYear(1950)

	if ratio <= 0.0 {
		t.Errorf("Could not properly calculate the ratio of terms used once!")
	}
	fmt.Println(ratio)
}
