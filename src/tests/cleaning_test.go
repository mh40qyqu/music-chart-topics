package tests

import (
	"fmt"
	"music-chart-topics/src/nlp"
	"strings"
	"testing"
)

func TestStopWordCheck(t *testing.T) {
	test1 := "and"
	test2 := "name"

	if nlp.CheckIfStopWord(&test1) == false {
		t.Error("'and' should be a stopword")
	}

	if nlp.CheckIfStopWord(&test2) == true {
		t.Error("'name' should not be be a stopword")
	}
}

func TestStopwordRemoval(t *testing.T) {
	test := "Hello my. name is max and I write this test."

	//'and' is a stopwords and should get removed
	nlp.RemoveStopwords(&test)

	if strings.Contains(test, "and") {
		t.Error("Stopword removal did not remove 'and'")
	}
	fmt.Println(test)
}

func TestBracketRemoval(t *testing.T) {
	test := "This is some music lyric with (extra) {extra} [genius] lyrics <or is it?>."

	nlp.RemoveBrackets(&test)

	if strings.Contains(test, "extra") || strings.Contains(test, "genius") || strings.Contains(test, "or is it?") {
		t.Error("bracket removal failed, some of the teststrings are still contained")
	} else {
		fmt.Println(test)
	}
}

func TestInterjectionRemoval(t *testing.T) {
	test := "Hello this is some genius lyric. Oh-oh-oh Yeah, this is going to rock so hard! oh,oh,oh,oh. ahhh-ahhhhhhhh-ahhh."

	nlp.RemoveInterjections(&test)

	if strings.Contains(test, "ahhh") || strings.Contains(test, "oh") {
		t.Error("'oh' or 'ahhh' should have been removed by the test!")
	}
	fmt.Println(test)
}

func TestDuplicateRemoval(t *testing.T) {
	test := "This is a simple test, na na na, but who even cares anymore. Oh oh oh oh. But is it enough of our project? Ahhhhh-ahhhhh-ahhhhh. Oh, oh, oh\nOh,  oh, oh\nOh,  oh, oh, oh, oh"

	nlp.RemoveDuplicates(&test)

	if strings.Contains(test, "oh") {
		t.Errorf("Oh wasnt recognized and removed as duplicate!")
	}
	fmt.Println(test)
}

func TestCleanLyricsOfYear(t *testing.T) {
	nlp.CleanAndSaveLyricsOfYear(1950)
}

func TestCleanAndSaveAllLyrics(t *testing.T) {
	nlp.CleanAndSaveAllLyrics()
}
