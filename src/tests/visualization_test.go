package tests

import (
	"fmt"
	"music-chart-topics/src/visualization"
	"testing"
)

func TestCreateTermCountGraph(t *testing.T) {
	visualization.CreateCountTermGraph("'em")
}

func TestCreateMovingWindowTokenTypeRatioDistributionGraph(t *testing.T) {
	visualization.CreateMovingWindowTokenTypeRatioDistributionGraph()
}

func TestCreateAllGraphs(t *testing.T) {
	fmt.Println("Creating all visualization graphs")
	visualization.CreateAllGraphs()
}
