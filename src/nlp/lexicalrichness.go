package nlp

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"music-chart-topics/src/util"
	"strings"
	"sync"
)

const (
	//StartYear marks the first year for which we have data
	StartYear = 1950
	//EndYear marks the last year for which we have data
	EndYear = 2019

	lexicalRichnessFilePath = "data/"
)

//LexicalRichnessData bundles all information we have about the lexical richness of a single year
type LexicalRichnessData struct {
	Year int

	AverageTermLength                 float64
	AverageTermLengthWithSepcialChars float64
	TermCount                         int
	TermMapSize                       int

	//track how many terms only occured once
	TermsOccuringOnce      int
	RatioTermsOccuringOnce float64

	LexicalRichness float64

	//this represents the termMap size of all years until this year
	TotalTermMapSize int
}

//implementing stringer interface
func (l LexicalRichnessData) String() string {
	return fmt.Sprintf("(Year: %d AverageTermLength: %f TermCount: %d TermMapSize: %d TermsOccuringOnce: %d LexicalRichness: %f TotalTermMapSize: %d)",
		l.Year, l.AverageTermLength, l.TermCount, l.TermMapSize, l.TermsOccuringOnce, l.LexicalRichness, l.TotalTermMapSize)
}

//CreateLexicalRichnessDataOfYear creates LexicalRichnessData struct for the given year
func CreateLexicalRichnessDataOfYear(year int) *LexicalRichnessData {
	lrd := LexicalRichnessData{Year: year}

	avgTermLength, avgTermLengthWithSpecialChars, termCount := GetAverageTermLengthAndTermCount(year)
	lrd.AverageTermLength = avgTermLength
	lrd.AverageTermLengthWithSepcialChars = avgTermLengthWithSpecialChars
	lrd.TermCount = termCount

	lrd.TermMapSize = len(CreateTermMapOfYear(year, true))
	lrd.TermsOccuringOnce = CountTermsOccureOnce(year)
	lrd.RatioTermsOccuringOnce = CalcRatioOfOneTimeUsedTermsOfYear(year)

	lrd.LexicalRichness = CalcLexicalRichness(year)
	lrd.TotalTermMapSize = GetTermMapGrowth()[year-StartYear]

	return &lrd
}

//CountTermsOccureOnce counts the number of terms occuring once
func CountTermsOccureOnce(year int) int {
	termMap := CreateTermMapOfYear(year, true)

	//calculate how many terms occure exactly once
	occureOnceCounter := 0
	for k := range termMap {
		if termMap[k] == 1 {
			occureOnceCounter++
		}
	}
	return occureOnceCounter
}

//CreateLexicalRichnessFile computes the lexical richness of all years and saves it to a file
func CreateLexicalRichnessFile() {
	fmt.Println("Creating lexical richness data file...")
	le := make([]LexicalRichnessData, EndYear-StartYear+1)

	wg := sync.WaitGroup{}

	for i := StartYear; i <= EndYear; i++ {
		go func(it int) {
			wg.Add(1)
			le[it-StartYear] = *CreateLexicalRichnessDataOfYear(it)
			wg.Done()
		}(i)
	}

	wg.Wait()

	err := writeListLexicalRichnessDataToJSON(lexicalRichnessFilePath, le)
	if err != nil {
		log.Println("CreateLexicalRichnessFile: ", err)
	}
	fmt.Println("Done creating file!")
}

//writeListLexicalRichnessDataToJSON writes a list of lexical richness data to the file as json
func writeListLexicalRichnessDataToJSON(path string, lrd []LexicalRichnessData) error {
	jsonFile := util.CreateFile(path + "LexicalRichness.json")
	if jsonFile != nil {
		file, _ := json.MarshalIndent(lrd, "", " ")
		return ioutil.WriteFile(jsonFile.Name(), file, 0644)
	}
	return fmt.Errorf("WriteListLexicalRichnessDataToJSON: could create lexical richness file")
}

//ReadLexicalRichnessData reads the lexical richness data in, returns an array containing the lexical richness of all years
func ReadLexicalRichnessData() []LexicalRichnessData {
	data, err := ioutil.ReadFile(lexicalRichnessFilePath + "LexicalRichness.json")
	if err != nil {
		log.Fatalln("ReadLexicalRichnessDataToJSON: Error when trying to read json file! -> ", err.Error())
	}

	var lrd []LexicalRichnessData

	err = json.Unmarshal(data, &lrd)
	if err != nil {
		log.Fatalln("ReadLexicalRichnessDataToJSON: Error when trying to parse json! -> ", err.Error())
	}

	return lrd
}

//GetAverageTermLengthAndTermCount reads in a cleaned lyrics json file and calculate the average length of terms with and without removal of special chars (like: ') and the total term count
func GetAverageTermLengthAndTermCount(year int) (float64, float64, int) {
	lyrics := util.ReadCleanedLyricsFromYear(year)
	termLength := 0.0
	termLengthWithSpecialChars := 0.0
	termCount := 0

	for _, lyric := range lyrics {
		terms := strings.Fields(lyric.Lyrics)

		if lyric.Lyrics == "None" || len(lyric.Lyrics) == 0 {
			continue
		}

		for _, term := range terms {
			if len(term) > 0 {
				//sry Denis :D
				temp := strings.ReplaceAll(term, ",", "")
				temp = strings.ReplaceAll(term, ".", "")
				temp = strings.ReplaceAll(term, "?", "")
				temp = strings.ReplaceAll(term, "!", "")
				termLengthWithSpecialChars += float64(len(temp))
			}

			//we want to reduce the term as much as possible here
			RemoveNonLetters(&term, "")

			if len(term) > 0 {
				termCount++
				termLength += float64(len(term))
			}
		}
	}
	return termLength / float64(termCount), termLengthWithSpecialChars / float64(termCount), termCount
}

//CreateTermMapOfYear creates a map of all occurences of words from the cleaned lyrics
func CreateTermMapOfYear(year int, removeNonLetters bool) map[string]int {
	lyrics := util.ReadCleanedLyricsFromYear(year)
	wordMap := make(map[string]int)

	for _, lyric := range lyrics {
		lyric.Lyrics = strings.ToLower(lyric.Lyrics)
		words := strings.Fields(lyric.Lyrics)

		if lyric.Lyrics == "None" || len(lyric.Lyrics) == 0 {
			continue
		}

		for _, word := range words {

			if removeNonLetters == true {
				RemoveNonLetters(&word, "")
			}

			if len(word) > 0 {
				if _, ok := wordMap[word]; ok == true {
					wordMap[word]++
				} else {
					wordMap[word] = 1
				}
			}
		}
	}
	return wordMap
}

//CalcLexicalRichness returns the lexical richness for the given year
func CalcLexicalRichness(year int) float64 {
	termMap := CreateTermMapOfYear(year, true)
	termCount := 0

	for k := range termMap {
		termCount += termMap[k]
	}

	return calcLexicalRichness(len(termMap), termCount)
}

//GetTermMapGrowth returns a list of ints, representing the number of new words for every year
func GetTermMapGrowth() []int {
	//Create list with size of our data
	newWordNumberList := make([]int, EndYear-StartYear+1)

	termMap := make(map[string]int)

	//merge the termMaps of all years and track the growth of the map
	for i := StartYear; i <= EndYear; i++ {
		termMap = mergeMaps(termMap, CreateTermMapOfYear(i, true))
		newWordNumberList[i-StartYear] = len(termMap)
	}
	return newWordNumberList
}

//calcLexicalRichnes calculates the lexical richness by a formula of Herdan from 1960 -> https://core.ac.uk/download/pdf/82620241.pdf
//this functions needs a number of unique words (types) and a number of all words of the measured corpus (tokens)
func calcLexicalRichness(uniqueTypes, allTokens int) float64 {
	var1 := math.Log(float64(uniqueTypes))
	var2 := math.Log(float64(allTokens))
	return var1 / var2
}

func mergeMaps(map1, map2 map[string]int) map[string]int {
	//we want to merge map2 into map1 and return map1
	for k := range map2 {
		if _, ok := map1[k]; ok == true {
			//key of map2 is in map1, so we just add the int values
			map1[k] += map2[k]
		} else {
			map1[k] = map2[k]
		}
	}
	return map1
}

//GetCountOfTerm returns the number of occurrences of the given term in the given year
func GetCountOfTerm(term string, year int) int {
	termMap := CreateTermMapOfYear(year, false)

	if val, ok := termMap[term]; ok == true {
		return val
	}
	return 0
}

func createListOfWords(year int) []string {
	lyrics := util.ReadCleanedLyricsFromYear(year)
	var wordList []string

	for _, lyric := range lyrics {
		lyric.Lyrics = strings.ToLower(lyric.Lyrics)
		words := strings.Fields(lyric.Lyrics)

		if lyric.Lyrics == "None" || len(lyric.Lyrics) == 0 {
			continue
		}

		for _, word := range words {
			RemoveNonLetters(&word, "")
			wordList = append(wordList, word)
		}
	}
	return wordList
}

func countNumberOfTypesInWordList(wordList []string) int {
	wordMap := make(map[string]int)

	for _, word := range wordList {
		if _, ok := wordMap[word]; ok == true {
			wordMap[word]++
		} else {
			wordMap[word] = 1
		}
	}

	return len(wordMap)
}

//CalcMATTR calculates the Moving Average Type-Token Ratio for the given year, where L is the parameter smaller than the text size.
func CalcMATTR(year int, L int) float64 {
	wordList := createListOfWords(year)
	iMax := len(wordList) - L
	if iMax <= 0 {
		log.Fatalln("CalcMATTR the L parameter needs to be less than the textsize")
	}

	typeCountArray := make([]int, len(wordList))

	for i := 0; i < iMax; i++ {
		typeCountArray[i] = countNumberOfTypesInWordList(wordList[i : i+L])
	}

	typeCountSum := 0
	for i := range typeCountArray {
		typeCountSum += typeCountArray[i]
	}

	return float64(typeCountSum) / float64(L*(len(wordList)-L))
}

//CalcMWTTR calculates the Moving Window Type-Token Ratio (MWTTR)
func CalcMWTTR(year int, L int) []float64 {
	wordList := createListOfWords(year)
	iMax := len(wordList) - L
	if iMax <= 0 {
		log.Fatalln("CalcMWTTR the L parameter needs to be less than the textsize")
	}

	typeTTRArray := make([]float64, len(wordList))

	for i := 0; i < iMax; i++ {
		typeTTRArray[i] = float64(countNumberOfTypesInWordList(wordList[i:i+L])) / float64(L)
	}

	return typeTTRArray
}

//CalcMWTTRD calculates the Moving Window Type-Token Ratio Distribution (MWTTRD)
func CalcMWTTRD(year int, L int) []float64 {
	wordList := createListOfWords(year)
	iMax := len(wordList) - L
	if iMax <= 0 {
		log.Fatalln("CalcMWTTRD the L parameter needs to be less than the textsize")
	}

	typeCountArray := make([]int, len(wordList))

	for i := 0; i < iMax; i++ {
		typeCountArray[i] = countNumberOfTypesInWordList(wordList[i : i+L])
	}

	ttrDistributionArray := make([]float64, L)

	//create distribution of mwttr
	for l := 1; l <= L; l++ {
		for _, typeCount := range typeCountArray {
			if typeCount == l {
				ttrDistributionArray[l-1]++
			}
		}
	}

	mapterms := CreateTermMapOfYear(year, true)

	for i := range ttrDistributionArray {
		ttrDistributionArray[i] /= float64(len(mapterms))
	}

	return ttrDistributionArray
}

//CalcRatioOfOneTimeUsedTermsOfYear calculats the ratio between terms occur one time and all used words.
//Because of zipfs law we know that generally the words on a text occur once is almost the half of the text with this function
//we can calculate the ratio for a given lyrics year.
func CalcRatioOfOneTimeUsedTermsOfYear(year int) float64 {
	typeCount := len(CreateTermMapOfYear(year, true))
	numberOfTermsOccuringOnce := CountTermsOccureOnce(year)
	return float64(numberOfTermsOccuringOnce) / float64(typeCount)
}
