package nlp

import (
	"bufio"
	"fmt"
	"music-chart-topics/src/util"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/wcharczuk/go-chart"
)

const zipfDir = "data/zipf/"

func generateWordCountMap(lyricsEntities [][]util.LyricsEntity, startyear int) map[string]int {

	wordmap := make(map[string]int)

	for year, ents := range lyricsEntities {
		fmt.Println("Calculating the year ", startyear+year)
		for _, ent := range ents {
			words := util.GetTokenList(ent.Lyrics)
			for _, word := range words {
				//is true when the map contains the word
				if val, ok := wordmap[word]; ok {
					wordmap[word] = val + 1
				} else {
					wordmap[word] = 1
				}
			}
		}
	}
	return wordmap
}

func printWordmap(wordmap map[string]int, filename string) {
	linesToPrint := make([]string, 0, len(wordmap))

	for word, count := range wordmap {
		line := word + "\t" + strconv.Itoa(count)
		linesToPrint = append(linesToPrint, line)
	}
	file, err := os.Create(zipfDir + filename)
	util.IsError(err)
	writer := bufio.NewWriter(file)
	n4, err := writer.WriteString(strings.Join(linesToPrint, "\n"))
	if err != nil {
		fmt.Println("There was a problem writing the file "+filename+" ; Error:", err)
	} else {
		fmt.Println("Sucessfully printed " + strconv.Itoa(n4) + " bytes to " + filename)
	}
}

//PrintAllZipf prints the zipf distribution of all the data
func PrintAllZipf() {
	filename := "allZipfGraph.png"
	lyricsEntities := util.ReadCleanedLyricsFromYearToYear(1950, 2019)
	wordmap := generateWordCountMap(lyricsEntities, 1950)
	fmt.Println("Number of words:", len(wordmap))
	//printWordmap(wordmap, filename)
	generateGraph(wordmap, filename)
}

func generateGraph(wordmap map[string]int, filename string) {

	dictSize := len(wordmap)
	counts := make([]float64, 0, len(wordmap))
	for _, count := range wordmap {
		counts = append(counts, float64(count))
	}

	ranking := make([]float64, dictSize)

	for i := 0; i < len(wordmap); i++ {
		ranking[i] = float64(i + 1)
	}

	sort.SliceStable(counts, func(i, j int) bool {
		return counts[i] > counts[j]
	})

	fmt.Println("Counts:")
	fmt.Println(counts)

	graph := chart.Chart{
		XAxis: chart.XAxis{
			Name: "Rank",
		},
		YAxis: chart.YAxis{
			Name: "Wordcount",
		},
		Series: []chart.Series{
			chart.ContinuousSeries{
				Style: chart.Style{
					StrokeColor: chart.GetDefaultColor(0).WithAlpha(64),
				},
				XValues: ranking,
				YValues: counts,
			},
		},
	}

	f, _ := os.Create(zipfDir + filename)
	defer f.Close()
	graph.Render(chart.PNG, f)
}
