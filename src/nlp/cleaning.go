package nlp

import (
	"music-chart-topics/src/util"
	"regexp"
	"strings"
	"sync"
)

const (
	stopwordsPath = "data/stopwords.txt"
)

var (
	bracketsRemovalRegexp     = regexp.MustCompile("(\\[.*?\\]|\\(.*?\\)|{.*?}|<.*?>)")
	interjectionRemovalRegexp = regexp.MustCompile("\\w{2,}(-\\w{2,}|, *\\w{2,}){2,}")
	specialCharRemovalRegexp  = regexp.MustCompile("[^a-zA-Z]")
	stopwords                 []string
)

//CleanAndSaveAllLyrics concurrently clean and save all lyrics from 1950 to 2019
func CleanAndSaveAllLyrics() {
	wg := sync.WaitGroup{}
	for i := 1950; i <= 2019; i++ {
		wg.Add(1)
		go func(year int) {
			CleanAndSaveLyricsOfYear(year)
			wg.Done()
		}(i)
	}
	wg.Wait()
}

//CleanAndSaveLyricsOfYear cleans the lyrics of year by removing brackets, stopwords, interjections and then saves them as json
func CleanAndSaveLyricsOfYear(year int) {
	util.WriteCleanedLyricsDataAsJSON(CleanLyricsOfYear(year, false), year)
}

//CleanLyricsOfYear returns a list of cleaned LyricsEntity, by removing brackets, stopwords and interjections
func CleanLyricsOfYear(year int, useStopwords bool) []util.LyricsEntity {
	lyricEnts := util.ReadLyricsFromYear(year)

	for it := range lyricEnts {
		if lyricEnts[it].Lyrics == "None" {
			continue
		}

		lyricEnts[it].Lyrics = strings.ToLower(lyricEnts[it].Lyrics)

		lyricEnts[it].Lyrics = strings.ReplaceAll(lyricEnts[it].Lyrics, ",", " ")

		RemoveBrackets(&lyricEnts[it].Lyrics)
		if useStopwords {
			RemoveStopwords(&lyricEnts[it].Lyrics)
		}
		RemoveInterjections(&lyricEnts[it].Lyrics)
		RemoveDuplicates(&lyricEnts[it].Lyrics)
	}
	return lyricEnts
}

//RemoveDuplicates shall remove words in sentences like ah ah ah ah
func RemoveDuplicates(lyric *string) {
	words := strings.Fields(*lyric)

	//keep track of current possible duplicate
	duplicateCounter := 0

	for it := range words {
		if it < len(words)-1 {
			leftNeightbour := strings.ToLower(specialCharRemovalRegexp.ReplaceAllString(words[it], ""))
			rightNeighbour := strings.ToLower(specialCharRemovalRegexp.ReplaceAllString(words[it+1], ""))
			if leftNeightbour == rightNeighbour {
				duplicateCounter++
				continue
			} else {
				//found at least a triple of duplicates
				if duplicateCounter > 1 {
					for i := 0; i <= duplicateCounter; i++ {
						words[it-i] = ""
					}
				}
				//reset duplicateCounter
				duplicateCounter = 0
			}
		} else {
			//check if duplicate is at the end of string
			if words[it] == words[it-1] && duplicateCounter > 1 {
				for i := 0; i <= duplicateCounter; i++ {
					words[it-i] = ""
				}
			}
		}
	}

	builder := strings.Builder{}
	for _, word := range words {
		if len(word) > 0 {
			builder.WriteString(word)
			builder.WriteString(" ")
		}
	}
	*lyric = builder.String()
}

//RemoveBrackets by applying a regex to the given lyrics strings, all brackets are removed ()[]{}<>, this function works in-place!
func RemoveBrackets(lyric *string) {
	*lyric = bracketsRemovalRegexp.ReplaceAllString(*lyric, "")
}

//RemoveInterjections removes all interjection of type ah, ah, ah/ ohhh-ohhh/ah,ah,ah/ etc., this functions works in-place!
func RemoveInterjections(lyric *string) {
	*lyric = interjectionRemovalRegexp.ReplaceAllString(*lyric, "")
}

//CheckIfStopWord checks if the given string is contained in the given stopwords list
func CheckIfStopWord(word *string) bool {
	//only read stopwords in one-time
	if len(stopwords) == 0 {
		stopwords = util.ReadInStopWordsFromFile(stopwordsPath)
	}

	for it := range stopwords {
		if strings.ToLower(*word) == strings.ToLower(stopwords[it]) {
			return true
		}
	}
	return false
}

//RemoveStopwords removes all stopwords from the given string, in-place
func RemoveStopwords(lyric *string) {
	//only read stopwords in one-time
	if len(stopwords) == 0 {
		stopwords = util.ReadInStopWordsFromFile(stopwordsPath)
	}

	wordList := strings.Fields(*lyric)

	lyricsBuilder := strings.Builder{}

	//this could be done quicker, but i think for our little data its ok
	for i, word := range wordList {
		word = specialCharRemovalRegexp.ReplaceAllString(word, "")

		for _, stopword := range stopwords {
			if strings.ToLower(word) == strings.ToLower(stopword) {
				goto end
			}
		}
		lyricsBuilder.WriteString(wordList[i])
		lyricsBuilder.WriteString(" ")
	end:
	}
	*lyric = lyricsBuilder.String()
}

//RemoveNonLetters removes all characters which are not letters
func RemoveNonLetters(lyric *string, replaceString string) {
	*lyric = specialCharRemovalRegexp.ReplaceAllString(*lyric, replaceString)
}
