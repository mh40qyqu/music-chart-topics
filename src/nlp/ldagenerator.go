package nlp

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"music-chart-topics/src/util"
	"regexp"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"time"

	"gonum.org/v1/gonum/mat"

	"gopkg.in/jdkato/prose.v2"

	"golang.org/x/exp/rand"

	"github.com/james-bowman/nlp"
)

var tagRegex = regexp.MustCompile("(?:N[NPS]{1,3}|J.{1,3}|V.{1,3})")

const topicDir = "data/topics/"

//TopicWord is a struct consisting of a word and its relevance in the topic
type TopicWord struct {
	Word string
	Rel  float64
}

//TopicStruct consists of a int and a array of TopicWords
type TopicStruct struct {
	Number int
	Words  []TopicWord
}

//PrintAnotherTopics uses james-bowmans nlp library
func printTopics(corpus []string, fileName string, stopwords ...string) {
	vectoriser := nlp.NewCountVectoriser(stopwords...)
	lda := nlp.LatentDirichletAllocation{
		Iterations:                    1000,
		PerplexityTolerance:           1e-2,
		PerplexityEvaluationFrequency: 30,
		BatchSize:                     100,
		K:                             100,
		BurnInPasses:                  1,
		TransformationPasses:          500,
		MeanChangeTolerance:           1e-5,
		ChangeEvaluationFrequency:     30,
		Alpha:                         0.02,
		Eta:                           0.002,
		RhoPhi: nlp.LearningSchedule{
			S:     10,
			Tau:   1000,
			Kappa: 0.9,
		},
		RhoTheta: nlp.LearningSchedule{
			S:     1,
			Tau:   10,
			Kappa: 0.9,
		},
		Rnd:       rand.New(rand.NewSource(uint64(time.Now().UnixNano()))),
		Processes: runtime.GOMAXPROCS(0),
	}
	fmt.Println("Starting generating the topics...")
	tfidf := nlp.NewTfidfTransformer()

	pipeline := nlp.NewPipeline(vectoriser, tfidf, &lda)

	_, err := pipeline.FitTransform(corpus...)
	if err != nil {
		fmt.Printf("Failed to model topics for documents because %v", err)
		return
	}

	topicsOverWords := lda.Components()
	//tr, tc := topicsOverWords.Dims()

	vocab := make([]string, len(vectoriser.Vocabulary))
	for k, v := range vectoriser.Vocabulary {
		vocab[v] = k
	}
	printTopicsToJSON(topicsOverWords, vocab, 10, fileName)
}

//GetTopicsFromDecade gets topics from a given year
func GetTopicsFromDecade(year int, stopwords ...string) {
	endyear := year + 9
	filename := "topics_" + strconv.Itoa(year) + "-" + strconv.Itoa(endyear) + ".json"
	lyricsEntities := util.ReadCleanedLyricsFromDecade(year)
	var size int
	for _, ents := range lyricsEntities {
		size += len(ents)
	}
	fmt.Println("Starting generating the corpus for years " + strconv.Itoa(year) + " to " + strconv.Itoa(endyear) + "...")
	fmt.Println("Number of Songs:", size)
	corpus := make([]string, 0, size)
	for _, entities := range lyricsEntities {
		for _, ent := range entities {
			doc, err := prose.NewDocument(ent.Lyrics)
			util.IsError(err)
			words := make([]string, 20)
			for _, token := range doc.Tokens() {
				// if the Tag matches the regex, its added to the document
				if tagRegex.MatchString(token.Tag) {
					words = append(words, token.Text)
				}
			}
			corpus = append(corpus, strings.Join(words, " "))
		}
	}
	fmt.Println("Done generating the corpus!")
	printTopics(corpus, filename, stopwords...)
}

//GetTopicsFromYear gets topics from a given year
func GetTopicsFromYear(year int, stopwords ...string) {
	filename := "topics_" + strconv.Itoa(year) + ".json"
	lyricsEntities := util.ReadCleanedLyricsFromYear(year)
	size := len(lyricsEntities)
	fmt.Println("Starting generating the corpus for year " + strconv.Itoa(year) + "...")
	fmt.Println("Number of Songs:", size)
	corpus := make([]string, 0, size)
	for _, ent := range lyricsEntities {
		doc, err := prose.NewDocument(ent.Lyrics)
		util.IsError(err)
		words := make([]string, 20)
		for _, token := range doc.Tokens() {
			// if the Tag matches the regex, its added to the document
			if tagRegex.MatchString(token.Tag) {
				words = append(words, token.Text)
			}
		}
		corpus = append(corpus, strings.Join(words, " "))

	}
	fmt.Println("Done generating the corpus!")
	printTopics(corpus, filename, stopwords...)
}

//GetTopicsFromAllYears gets topics from a given year
func GetTopicsFromAllYears() {
	stopwords := append(util.ReadInStopWordsFromFile("data/topicStopwords.txt"), util.ReadInStopWordsFromFile("data/anotherTopicStopwords.txt")...)
	filename := "topics_complete.json"
	lyricsEntities := util.ReadCleanedLyricsFromYearToYear(1950, 2019)
	var size int
	for _, ents := range lyricsEntities {
		size += len(ents)
	}
	fmt.Println("Starting generating the corpus for all the years...")
	fmt.Println("Number of Songs:", size)
	corpus := make([]string, 0, size)
	for _, entities := range lyricsEntities {
		for _, ent := range entities {
			doc, err := prose.NewDocument(ent.Lyrics)
			util.IsError(err)
			var words []string
			for _, token := range doc.Tokens() {
				// if the Tag matches the regex, its added to the document
				if tagRegex.MatchString(token.Tag) {
					words = append(words, token.Text)
				}
			}
			corpus = append(corpus, strings.Join(words, " "))
		}
	}
	fmt.Println("Done generating the corpus!")
	printTopics(corpus, filename, stopwords...)
}

//GenerateAllTopics generates the topics and prints them to a json file
func GenerateAllTopics() {
	stopwords := append(util.ReadInStopWordsFromFile("data/topicStopwords.txt"), util.ReadInStopWordsFromFile("data/anotherTopicStopwords.txt")...)
	for year := 1950; year <= 2019; year = year + 10 {
		GetTopicsFromDecade(year, stopwords...)
	}
}

func printTopicsToJSON(components mat.Matrix, vocab []string, topn int, filename string) {
	tr, tc := components.Dims()
	topics := make([]TopicStruct, 0, 100)

	for topic := 0; topic < tr; topic++ {
		words := make([]int, tc)
		for word := 0; word < tc; word++ {
			words[word] = word
		}
		topicWords := make([]TopicWord, topn)
		//order by the relevance (relevant first) and then print out topn words of the topic
		sort.SliceStable(words, func(i, j int) bool { return components.At(topic, words[i]) > components.At(topic, words[j]) })
		for i, word := range words {
			if i < topn {
				topicWords[i] = TopicWord{vocab[word], components.At(topic, word)}
			}
		}
		//fmt.Println(topicWords[:])
		if len(topicWords) > 0 {
			topics = append(topics, TopicStruct{topic, topicWords[:]})
		} else {
			fmt.Println("No correct topic...")
		}
	}
	filepath := topicDir + filename
	util.CreateFile(filepath)
	fileContent, _ := json.MarshalIndent(topics, "", " ")
	err := ioutil.WriteFile(filepath, fileContent, 0644)
	util.IsError(err)
}
